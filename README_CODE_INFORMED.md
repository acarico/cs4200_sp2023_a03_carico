# Austin Carico



## Bachelor of Science in Computer Science, specialized in Computational Data Science

<b> How long it takes to complete this assignment? </b> <br> 
Roughly 4 hours total.
<br> <br>

<b> What is the most challenging part for you? </b> <br>
I didn't understand the distance calculations too well and I had a hard time figuring out how the algorithms compare without using the metrics that were used in the demo.
<br> <br>

<b>Compare uninformed search and informed search and indicate the major difference between them. </b> <br>
Informed search knows the path in front of it while uninformed does not, with the major difference being how informed search can use goal states to estimate the distance from the goal, while uninformed does not have this benefit.

<br> <br>


 <b>Explain what is admissible heuristic and why it is important.</b> <br> 
 An admissible heuristic is an uderestimate of the actual true path to the goal, it is important because the less efficient goal cost can seem less than what the estimated best goal cost is, if the heuristic were to be overestimated.
<br><br>
 <b> Given a set of admissible heuristics, what is the definition of dominance and how dominance impacts informed search?</b> <br>
The dominance of a set of admissible heuristics of the max of the set, and it is a better choice for an informed search algorithm.
 <br> <br>
