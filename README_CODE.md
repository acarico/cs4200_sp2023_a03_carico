# Austin Carico



## Bachelor of Science in Computer Science, specialized in Computational Data Science

<b> How long it takes to complete this assignment? </b> <br> 
I would say it took me about 3 hours for this assignment, total.
<br> <br>

<b> What is the most challenging part for you? </b> <br>
I have a hard time understanding how straight line distance works in terms of where to place the node locations, so I couldn't get the graph to look like the assignment.<br> <br>

<b>What do you learn from the visualization? </b> <br>
The visualizations showed me the paths that each algorithm wants to take when finding the goal state to be in. <br> <br>

<b> Compare graph search and tree search and indicate the major difference between them. </b> <br>
Graph search and tree search both go through a hierarchy of nodes and expand them to view their children in order to find the best solution based on the search algorithm used. The major difference between them is tree search can have previously explored nodes inside its frontier, but graph search makes sure that the frontier does NOT have any of these previously explored nodes in the frontier.
 <br> <br>

 <b> In the code, what are the differences between implementing tree search and graph search? </b> <br>
The implementation of tree search sets the code to check for the goal while there are still nodes within the frontier, and does not check these nodes in the frontier when it looks runs the goal test, while in the graph search problem, it has its own explored variable which stores a set of nodes that were explored, that then get implemented in checking if the and expanded child nodes are in the frontier as well as in the explored set. The tree search implentation does not run this check and instead only checks for the goal state, otherwise it just expands the current node.
 <br> <br>
