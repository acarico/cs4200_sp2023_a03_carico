# Austin Carico



## Bachelor of Science in Computer Science, specialized in Computational Data Science

<b> How long it takes to complete this assignment? </b> <br> 
It took me roughly 3-4 hours for this assignment, but I did it split in 2 days so maybe an hour one day and 2 hours the next is the best estimate.
<br> <br>

<b> What is the most challenging part for you? </b> <br>
I don't understand how to translate a state space graph into a tree to perform a tree search so I had a hard time knowing which node was a child of what node. <br> <br>

<b> Compare BFS, DFS, and uniform cost search in terms of completeness, time complexity, space complexity, and optimality. </b> <br>
BFS: It is a complete search given the maximum branching factor is not infinite. Time is O(b^(d+1)) and space is the same as time. It is optimal if cost is not a factor(i.e, each cost from one state to the next is the same)
<br>DFS: Infinite spaces can happen with  loops or infinite depth, so it is incomplete unless the space it is in is finite. Time is O(b^m), could be slower than BFS when the maximum depth of the space is larger than the depth of the least cost solution, but DFS can be faster if there is not much gap between the two. Space is O(bm) which is faster than BFS. This is not optimal generally.
<br>Uniform cost search: It is complete if all actions don't have the same cost(it becomes breadth first search if so). Time and space are both O(C*/cl+1) with l being the limit that is found on the depth. Since uniform cost search becomes BFS when all costs are equal, this makes uniform cost search a more optimal BFS if costs are a factor. Both are more optimal than DFS, and both have more scenarios where they are complete than DFS as well, however DFS is still faster than them if the solution node is, for example, the left most and deepest node on the tree. <br> <br>

<b> Based on your understanding, indicate the best suitable uninformed search strategy (BFS, DFS, or uniform search) for the following three situations: 1) equal action cost and dense solution space, 2) equal action cost and d is significantly smaller than m, and 3) actions with different costs. Justify the reasons for your answers. </b> <br>
1\) With a denser solution space and no costs to consider, DFS would be better than BFS and uniform cost search considering it would find the solution that's located at large depth.
<br>2\) Since the least cost solution depth is smaller than the maximum, BFS is the best option since it would be finding the shallowest option in an already shallow solution.
<br>3)Uniform cost search is similar to BFS but it is considering costs unlike BFS, and DFS may be cycling through several deep depths before finding a solution, so uniform cost search would be the best choice here.
 <br> <br>
