# Austin Carico



## Bachelor of Science in Computer Science, specialized in Computational Data Science

<b> How long it takes to complete this assignment? </b> <br> 
I'd say it took me roughly 3 hours.
<br> <br>

<b> What is the most challenging part for you? </b> <br>
I had a hard time putting the visual of arc consistency into words because I wasn't sure how one writes that problem out.<br> <br>

<b>Compare traditional search and CSPs then indicate the suitable situations for CSPs. </b> <br>
Traditional search uses goal testing, initial states and successor functions in their formulation whereas CSPs uses variables, domains, and constraints, and they detect failures using the constraints they set. They typically are faster than traditional search algorithms, such as depth first search. The most suitable situation to use a CSP is when there are a set of rules that have to be followed in order to solve a problem, and those rules can be respected by a CSP problem.
<br> <br>

<b>Based on your understanding, give a few real-world problems that can be solve by CSPs and justify why CSPs would be a better option by traditional search.  </b> <br>
Finding an optimal solution to building a computer would be better than a traditional search because some computer components work better alongside other components, or they can only function when they are next to other components (Ryzen CPUs can only work with the compatible motherboard for example). A crossword puzzle is another example, because each word in the crossword intersects with another word(in most cases). Having these intersecting letters as constraints would make a CSPs the better option over traditional search.
 <br> <br>
